
 o new implementation of constructive reals in module Cr
   (Java library by Hans Boehm ported to ocaml); this one is usually more 
   efficient than Creal (uses less memory).
   The two interfaces are (almost) identical, so it is easy to switch from
   one to the other.
 o slight changes in Creal interface to be compatible with Cr interface:
   - of_string : ?radix:int -> string -> t
   - log: ~base:t -> t -> t

Version 0.6, 10/05/2005
=======================
 o RPM package contributed by Julien Narboux (ftp://ftp.mandriva.com/incoming/)
 o fixed installation issues (now uses ocamlmklib);
   no need to link with gmp.cmxa anymore (see README)
 o improved arctan near 1 (contribution by Roland Zumkeller)
 o improved min and max (contribution by Roland Zumkeller)

Version 0.5, 29/11/2004
=======================
 o added min and max
 o fixed bug in arccos/arcsin (discontinuity)

Version 0.4, 25/2/2003
======================
 o integration of release 2002/11/23 of mlgmp 
   (fixing a GC bug during integer division)
 o new module Cmpf: parallel use of floats and exact reals
 o fixed bug in multiplication algorithm (found while doing a formal proof!)

Version 0.3, 11/4/2002
======================
 o toplevels pretty-printers now trap and print errors
 o improved Creal.to_string (now has a clear specification)
 o integration of release 2002/03/06 of mlgmp

Version 0.2, 9/4/2002
=====================
 o libraries (.cma/.cmxa) instead of single .cmo/.cmx files;
   installation in subdir creal/ of ocaml stdlib
 o sanity checks with "make test", benchmark with "make bench"
 o acceleration of arctan when |x|>=1
 o bug fixed: non-termination of arcsin(1) or arccos(0) 
 o customized toplevels ocamlgmp and ocamlcreal

Version 0.1, 2/11/2001
======================
 o first public release
