
open Cr
open Cr.Infixes

let x = of_int 1
let y = of_int 7
let r = sin (sin (sin one))

(* "exp(-pi) = exp(-pi/2) * exp(-pi/2)"
	  (exp (neg pi)) (let y = exp (neg half_pi) in y *! y)
*)

let r = mul (of_int 4) (arctan one)

let () = Format.printf "%F@." (to_float r 50)
let () = print_endline (to_string ~radix:10 r 100)

