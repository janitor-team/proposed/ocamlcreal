
Creal for Objective caml: Exact real arithmetic.
Copyright (C) 2000 Jean-Christophe Filli�tre.

This  library implements  exact  real  arithmetic.
There are actually two different implementations:

- Module Creal,   following  Val�rie
  M�nissier-Morain Ph.D. thesis (http://www-calfor.lip6.fr/~vmm/).

  A  real  x  is  represented  as  a function  giving,  for  any  n,  an
  approximation zn/4^n of x  such that |zn/4^n - x| < 1,  where zn is an
  arbitrary precision integer (of type Gmp.Z.t).

- Module Cr, a translation of Hans Boehm's Java library CR
  (see http://www.hpl.hp.com/personal/Hans_Boehm/crcalc/)

The two implementations have almost identical interfaces, so that it is easy 
to switch from one to the other.

Coercions from type int, Gmp.Z.t, Gmp.Q.t, basic operations (addition,
subtraction,  multiplication,   division,  power,  square   root)  and
transcendental  functions (sin,  cos, tan,  log, exp,  arcsin, arccos,
etc.) and a few constants (pi, e) are provided.

A small reverse-polish calculator is provided to test the library
(ecalc.opt).


USAGE
=====

Compilation of a program with a single module Foo (in foo.ml) using Creal
(assumes Creal is installed; see INSTALL).

Bytecode:

	ocamlc -c -I +creal foo.ml
	ocamlc -o foo -I +creal creal.cma foo.cmo

Native-code:

	ocamlopt -c -I +creal foo.ml
	ocamlopt -o foo -I +creal creal.cmxa foo.cmx
