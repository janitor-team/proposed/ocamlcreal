
(** Comparison between floating-point numbers and exact arithmetic *)

open Gmp

type t 
  
val add : t -> t -> t
val neg : t -> t
val sub : t -> t -> t

val abs : t -> t
    
val mul : t -> t -> t
val inv : t -> t
val div : t -> t -> t
    
val pow_int : t -> int -> t
    
val sqrt : t -> t

val ln : t -> t
    
val exp : t -> t
val pow : t -> t -> t

val sin : t -> t
val cos : t -> t
val tan : t -> t
    
val arcsin : t -> t
val arccos : t -> t
val arctan : t -> t

val arctan_reciproqual : int -> t

val sinh : t -> t
val cosh : t -> t
val tanh : t -> t
    
val zero : t
val one : t
val two : t

val pi : t
val pi_over_2 : t

val e : t

val cmp : t -> t -> int

(*s Coercions *)

val of_int : int -> t
val of_z : Z.t -> t
val of_q : Q.t -> t
val of_float : float -> t
val of_string : string -> t

val to_float : t -> int -> float
val to_q : t -> int -> Q.t

(*s Pretty-print *)

val set_precision : int -> unit

val to_string : t -> string

val pp : Format.formatter -> t -> unit

(*s Infix operators *)

module Infixes : sig
  val ( + ) : t -> t -> t
  val ( - ) : t -> t -> t
  val ( * ) : t -> t -> t
  val ( / ) : t -> t -> t
end
