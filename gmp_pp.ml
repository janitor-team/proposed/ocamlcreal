
open Gmp
open Format

let z z = print_string (Z.string_from z)

let q q = 
  let n = Q.get_num q
  and d = Q.get_den q in
  print_string (Z.string_from n);
  if Z.cmp_si d 1 != 0 then printf "/%s" (Z.string_from d);;

