(*
 * Exact calculator.
 * Copyright (C) 2001 Jean-Christophe FILLIATRE
 * 
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License version 2, as published by the Free Software Foundation.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * See the GNU Library General Public License version 2 for more details
 * (enclosed in the file LGPL).
 *)

(*i $Id: calc.mll,v 1.6 2005/10/26 09:25:06 filliatr Exp $ i*)

{

  open Lexing
  open Printf
  open Cr

(*s Options and parsing of the command-line. *)

  let precision = ref 10

  let usage = "usage: ecalc [-p prec]

ecalc is a reverse-polish exact calcultator

Copyright (c) 2001- Jean-Christophe Filli�tre
This is free software with ABSOLUTELY NO WARRANTY

commands are
  k, prec      pops the top of stack and uses it as precision 

  ;, p, print  displays top of stack
  n, popd      pops top of stack and displays it
  f, show      displays the whole stack
  
  pop          pops top of stack
  c, clear     clears the stack
  d, dup       duplicates top of stack
  r, swap      swaps the two elements on top of stack

constants are
  pi, e

binary operations are
  +, -, *, /, log ('x y log' is log_x(y)), pow (or ^)

unary operations are
  ~ (negation), i (inverse), sqrt (or v), sin, cos, tan, ln, exp, 
  arcsin, arccos, arctan, sinh, cosh, tanh, arcsinh, arccosh, arctanh

options are"

  let speclist = 
    ["-p", Arg.Int (fun n -> precision := n), " set decimal precision"]

  let _ = Arg.parse speclist (fun _ -> ()) usage

(*s The stack. *)

  let stack = ref []

  let push x = stack := x :: !stack

  let error msg = printf "%s\n" msg; flush stdout

  let pop () = match !stack with
    | [] -> invalid_arg "pop"
    | x :: l -> stack := l; x

  let display_stack () =
    List.iter
      (fun x -> printf "   %s\n" (to_string x !precision)) 
      (List.rev !stack);
    flush stdout 

(*s Unary and binary operations on the stack. *)

  let unop f = push (f (pop ()))

  let binop f =
    let x2 = pop () in
    let x1 = pop () in
    push (f x1 x2)

}

(*s Commands are parsed with a lexer. *)

let digit = ['0'-'9']
let constant = digit+ | (digit* '.' digit+ | digit+ '.' digit*)

rule loop = parse
  | [' ' '\t' '\n']+ 
    { loop lexbuf }

  | "help"
    { Arg.usage speclist usage; flush stderr }
  | "k" | "prec" | "precision"
    { precision := Gmp.Z.int_from (approx (pop ()) 0) }
  | "K" | "pushprec"
    { push (of_int !precision) }

  | "f" | "show" | "stack"
    { display_stack () }
  | "n" | "popd"
    { let x = pop () in 
      printf "   %s\n" (to_string x !precision); flush stdout }
  | ";" | "p" | "print"
    { match !stack with
	| [] -> 
	    error "<empty stack>"
	| x :: _ ->
	    printf "   %s\n" (to_string x !precision); flush stdout }

  | "pop"
    { ignore (pop ()) }
  | "c" | "clear"
    { stack := [] }
  | "d" | "dup"
    { let x = pop () in push x; push x }
  | "r" | "swap"
    { let x = pop () in let y = pop () in push x; push y }
  | "z" | "pushstackdepth"
    { push (of_int (List.length !stack)) }

  | constant             
    { push (of_string (lexeme lexbuf)) }
  | "pi" { push pi }
  | "e"  { push e }

  | "+"   { binop add }
  | "-"   { binop sub }
  | "*"   { binop mul }
  | "/"   { binop div }
  | "~"   { unop neg }
  | "i"   { unop inv }

  | "sin"  { unop sin }
  | "cos"  { unop cos }
  | "tan"  { unop tan }
  | "arcsin"  { unop arcsin }
  | "arccos"  { unop arccos }
  | "arctan"  { unop arctan }

  | "exp"  { unop exp }
  | "ln"   { unop ln }
  | "log"  { binop (fun base -> log ~base) }
  | "^" | "pow"  { binop pow }

  | "sinh"  { unop sinh }
  | "cosh"  { unop cosh }
  | "tanh"  { unop tanh }
  | "arcsinh"  { unop arcsinh }
  | "arccosh"  { unop arccosh }
  | "arctanh"  { unop arctanh }

  | "v" | "sqrt" { unop sqrt }

  | eof { raise End_of_file }
  | _   { raise Parsing.Parse_error }

{

(*s The main program is an infinite loop exiting on [End_of_file]. *)

  let _ = Sys.catch_break true

  let main () =
    let lb = from_channel stdin in
    try
      while true do 
	try
	  loop lb 
	with 
	  | Sys.Break -> error "<interrupted>"
	  | Parsing.Parse_error -> error "<syntax error>"
	  | Invalid_argument "pop" -> error "<empty stack>"
      done
    with End_of_file ->
      flush stdout; exit 0
      
  let _ = Printexc.catch main ()

}
